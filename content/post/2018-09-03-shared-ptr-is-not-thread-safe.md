---
title: std::shared_ptr is not thread safe
date: 2018-09-03
tags: ["c++", "multithread"]
---

[std::shared_ptr is not thread safe](https://marsettler.com/2018/09/03/shared-ptr-is-not-thread-safe.html)
